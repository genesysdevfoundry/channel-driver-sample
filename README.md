#Channel Sample#

This is a sample channel driver for Genesys Social Messaging Server.

### What it contains ###

* A compiled channel driver with configuration and a routing business process
* Source code for the sample channel driver
* Channel Driver API JavaDocs
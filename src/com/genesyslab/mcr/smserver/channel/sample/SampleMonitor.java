/**
 * File:    SampleMonitor.java
 * Project: Channel_Sample
 *
 * Copyright (C) 2011-2013 Genesys Telecommunications Laboratories Inc.
 *
 * January 05, 2011
 */
package com.genesyslab.mcr.smserver.channel.sample;

import com.genesyslab.mcr.smserver.channel.ChannelDriverConstants;
import com.genesyslab.mcr.smserver.channel.ChannelConnectors.InboundRoute;
import com.genesyslab.mcr.smserver.common.SrvUtil;
import com.genesyslab.mcr.smserver.gframework.FieldNames;
import com.genesyslab.mcr.smserver.gframework.LmsMessages;
import com.genesyslab.platform.commons.collections.KeyValueCollection;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.openmedia.protocol.interactionserver.requests.interactionmanagement.RequestSubmit;

/**
 * <p>
 * Genesys SM Server - Sample Media Channel Driver
 * <p>
 * Imitates inbound data flow from a media source, i.e. submits interactions to Interaction Server.
 * <p>
 * Copyright (C) 2011-2013 Genesys Telecommunications Laboratories Inc.
 * <p>
 * All rights reserved.
 * 
 * @version 8.1.4
 */
public class SampleMonitor
	extends Thread
{
    private SampleDriver sampleDriver;
    private SampleDriverParams driverParams = null;

    private long lastMsgsFetchTime = 0;

    /**
     * @param SampleMonitor
     */
    public SampleMonitor(SampleDriver sampleDriver)
    {
	super("SampleMonitor");
	setDaemon(true);

	this.sampleDriver = sampleDriver;
	this.driverParams = sampleDriver.driverParams;

	final String logRpfx = sampleDriver.logPfx("SampleMonitor.<init>");
	sampleDriver.getGfrPU().trace(LmsMessages.called, logRpfx, "");
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Thread#run()
     */
    public void run()
    {
	final String logRpfx = sampleDriver.logPfx("SampleMonitor.run");

	lastMsgsFetchTime = System.currentTimeMillis() - sampleDriver.driverParams.samplingPeriod * 1000;
	int messagesCreated = 0;

	while (true) {
	    // Check the driver's connection state -------------------------------------------------
	    if (!sampleDriver.mediaConnected) {
		try {
		    sleep(1000);
		}
		catch (InterruptedException ex) {
		    sampleDriver.getGfrPU().trace(LmsMessages.exception2, logRpfx, SrvUtil.getExcLogRecord(ex));
		}
		continue;
	    }

	    // Control messages' generation periodicity --------------------------------------------
	    if (System.currentTimeMillis() - lastMsgsFetchTime < sampleDriver.driverParams.samplingPeriod * 1000) {
		try {
		    sleep(1000);
		}
		catch (InterruptedException ex) {
		    sampleDriver.getGfrPU().trace(LmsMessages.exception2, logRpfx, SrvUtil.getExcLogRecord(ex));
		}
		continue;
	    }

	    // Form and submit a message------------------------------------------------------------
	    if (messagesCreated < driverParams.messagesCreateMax) {
		try {
		    ++messagesCreated;

		    String msgId = "SAMPLE-" + 1000000 + messagesCreated;
		    KeyValueCollection messageData = new KeyValueCollection();
		    messageData.addString(SampleDriver.KEY_PFX + ChannelDriverConstants.UMSKEY_MsgType,
					  SampleDriver.MSGTYPE_SAMPLE);
		    messageData.addString(SampleDriver.KEY_PFX + ChannelDriverConstants.UMSKEY_MsgId, msgId);

		    // SM Server's required fields =====================================================
		    messageData.addString(FieldNames.UMS_Channel, sampleDriver.channelPU.getChannelName());
		    messageData.addString(ChannelDriverConstants.UMSKEY_FromAddr, "from-sample-driver");
		    messageData.addString(ChannelDriverConstants.UMSKEY_ToAddr, "to-interaction-server");
		    messageData.addUTFString(ChannelDriverConstants.UMSKEY_MsgPlainText, "sample_text_" + msgId);
		    // =================================================================================

		    sampleDriver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
						  "data from sample...\n" + messageData.toStringLine());

		    // Submit message ============================================
		    submitAndWait(messageData);
		    // ===========================================================
		}
		catch (Exception ex) {
		    sampleDriver.getGfrPU().trace(LmsMessages.exception2, logRpfx, SrvUtil.getExcLogRecord(ex));
		}

		lastMsgsFetchTime = System.currentTimeMillis();
	    }
	}
    }

    /**
     * The method submits request to Interaction Server and waits for a response from it. If there is no connection to
     * Interaction Server exists, the method tries to repeat the request up to driverParams.itxAttemptsMax times with
     * interval driverParams.itxRepeatTimeout seconds.
     * 
     * @param messageData
     *            content of a message to send
     * @param messageMediaType
     *            interaction media type
     * @return true - successfully sent request, false - failed to send
     * @throws Exception
     *             if failed to deliver request to Interaction Server
     */
    private boolean submitAndWait(KeyValueCollection messageData)
	    throws Exception
    {
	final String logRpfx = sampleDriver.logPfx("submitAndWait");

	// Form RequestSubmit object (refer to Genesys Platform SDK) ...............................

	// Set interaction attributes
	RequestSubmit requestSubmit = RequestSubmit.create(driverParams.inboundMedia, driverParams.itxType);
	requestSubmit.setInteractionSubtype(driverParams.itxSubType);
	requestSubmit.setReferenceId(sampleDriver.channelPU.getNextItxReferenceId());
	requestSubmit.setUserData(messageData);
	// Get and set tenant ID and interaction queue name
	InboundRoute inbRoute = sampleDriver.channelPU.getChannelConnector().getInboundRoute();
	requestSubmit.setTenantId(inbRoute.tenantId);
	requestSubmit.setQueue(inbRoute.pagingQName);

	// Print the request to a log file
	sampleDriver.getGfrPU().trace(LmsMessages.sent_data, logRpfx, "itx request...\n" + requestSubmit);

	// Assign a unique reference ID to the request
	int refID = requestSubmit.getReferenceId();

	// Send the request to Interaction Server ..................................................
	int attemptNum = 0; // the request's send attempt number

	while (true) {
	    try {
		++attemptNum;

		Message responseMsg = sampleDriver.channelPU
			.sendItxRequestAndWait(requestSubmit, 1000 * driverParams.itxSubmitTimeout);
		if (null == responseMsg) // failed to get a response from Interaction Server during specified timeout
		    throw new Exception("timeout on sendItxRequestAndWait");

		// Print a response received from Interaction Server
		sampleDriver.getGfrPU().trace(LmsMessages.received_data, logRpfx, "itx response...\n" + responseMsg);
		return true;
	    }
	    catch (IllegalStateException ex) {
		if (attemptNum >= driverParams.itxResubmitAttempts) {
		    sampleDriver.getGfrPU().trace(LmsMessages.exception2, logRpfx, SrvUtil.getExcLogRecord(ex));

		    String logRec = String
			    .format("failed to submit request, stop attempts because exceded max %d allowed, refID=%d",
				    attemptNum, driverParams.itxResubmitAttempts, refID);
		    sampleDriver.getGfrPU().trace(LmsMessages.error2, logRpfx, logRec);

		    return false;
		}

		sampleDriver.getGfrPU().trace(LmsMessages.exception2, logRpfx, ex.getMessage());
	    }
	    catch (Exception ex) { // ProtocolException
		sampleDriver.getGfrPU().trace(LmsMessages.exception2, logRpfx, SrvUtil.getExcLogRecord(ex));
		return false;
	    }

	    // ......................................................................................
	    String logRec = String
		    .format("failed to submit request,"
				    + " will repeat request attempt %d from %d allowed in %d seconds, refID=%d",
			    attemptNum + 1, driverParams.itxResubmitAttempts, driverParams.itxResubmitDelay, refID);
	    sampleDriver.getGfrPU().trace(LmsMessages.error2, logRpfx, logRec);
	    sleep(1000 * driverParams.itxResubmitDelay);
	}
    }
}

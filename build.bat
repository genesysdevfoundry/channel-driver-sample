@echo Genesys SM Server - Sample Driver for SM Server Build Procedure 

@echo ====================================================================== 
@echo           Java SDK 1.7 is used to build the project  
@echo ====================================================================== 

@rem !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@rem - Place this build.bat file in appropriate folder, where the build procedure will be run
@rem - Copy the sample's source files with its folders structure to the current directory
@rem - Set variable GCTI_JDK_PATH with JDK's location (where files javac.exe and jar.exe are located)
@rem - Set variable BUILD_LIBS with Social Messaging Server's folder 'lib' location
@rem - Run build.bat file
@rem - Get the build's result (file channel-sample.jar) from folder 'release'


@set GCTI_JDK_PATH=C:\Program Files\Java\jdk1.7.0_55\bin
@set BUILD_LIBS=C:\Program Files\GCTI\eServices 8.5.0\Genesys Social Messaging Server\SmServer_85_sbbelovdt\lib
@rem !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

@echo : GCTI_JDK_PATH    : %GCTI_JDK_PATH%
@echo : BUILD_LIBS       : %BUILD_LIBS%

@echo ===== build classes =====
md classes
md release
cd src

@set BUILD_CLASS_PATH=%BUILD_LIBS%\smserver.jar;%BUILD_LIBS%\genesys\commons.jar;%BUILD_LIBS%\genesys\kvlists.jar;%BUILD_LIBS%\genesys\openmediaprotocol.jar;%BUILD_LIBS%\genesys\protocol.jar;

"%GCTI_JDK_PATH%\javac.exe" -classpath "%BUILD_CLASS_PATH%" -d ..\classes com\genesyslab\mcr\smserver\channel\sample\*.java

echo ===== Archiving classes =====

cd ..
cd classes
"%GCTI_JDK_PATH%\jar.exe" cvfm ..\release\channel-sample.jar  ..\src\META-INF\manifest.mf com\genesyslab\mcr\smserver\channel\sample\*.class
cd ..

pause
